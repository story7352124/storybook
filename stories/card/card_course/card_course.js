import './card_course.scss';
import {createHeading} from "../../header/heading/header";
import {createButton} from "../../button/button";
import {createContent} from "../../header/content/content";

export const createCard = () => {
    const linkCourse = document.createElement('a')
    linkCourse.className = [
        'text-decoration-none'
    ]
    linkCourse.href = [
        '#'
    ]
    const container = document.createElement('div');
    container.className = [
        'mw-250',
        'col-md-6',
        'col-xl-4',
        'my-3',
        'list-item',
    ].join(' ');

    const mainText = createHeading({
        size: 3,
        text: "VBAG01: Tuyệt đỉnh VBA - Viết code trong tầm tay",
    });
    mainText.className = [
        'fw-700',
        'fs-16',
        'price-color',
    ].join(' ');

    const secondText = createContent({
        text: "Khóa học VBA Excel này sẽ giúp bạn thành thạo VBA giúp tự động hóa quy trình, công việc trên Excel, tối ưu hiệu quả, tăng 200% năng suất làm việc.",
    });
    secondText.className = [
        'line-clamp',
        'line-clamp-2-lines',
        'text-content-color',
    ].join(' ');
    const rateText = createContent({
        text: "4.85"
    });
    rateText.className = [
        'me-2',
        'color_rate',
    ].join(' ')
    const countRateText = createContent({
        text: "(72)"
    });
    countRateText.className = [
        'third-color',
    ];
    const countUsers = createContent({
        text: "18,390 học viên"
    });

    countUsers.className = [
        'third-color'
    ];
    const priceSaleCourse = createContent({
        text: "499,000đ",
    });
    priceSaleCourse.className = [
        'my-auto',
        'me-3',
        'fs-18',
        'fw-500',
        'price-color',
    ].join(' ')
    const priceCourse = createContent({
        text: "799,000đ",
    });
    priceCourse.className = [
        'text-through',
        'third-color',
        'my-auto'
    ].join(' ')
    const iconStar = document.createElement('i');
    iconStar.className = [
        'bi',
        'bi-star-fill',
        'me-2',
        'color_rate',
    ].join(' ')
    const courseImg = document.createElement('img');
    courseImg.src = [
        'https://gitiho.com/caches/cc_medium/cou_avatar/2021/05_21/image_05d37a08d453167e96fe8546df55bfd2.jpg',
    ].join(' ');
    courseImg.className = [
        'w-100'
    ].join(' ')


    linkCourse.append(container);
    container.append(courseImg);

    const container2 = document.createElement('div');
    container2.className = [
        'px-3',
        'pt-3',
    ].join(' ');
    container.append(container2);
    container2.append(mainText);
    container2.append(secondText);
    const container2_1 = document.createElement('div');
    container2.append(container2_1);
    const container2_1_1 = document.createElement('div');
    container2_1.append(container2_1_1)
    container2_1.className = [
        'd-flex',
        'justify-content-between',
    ].join(' ');
    container2_1_1.append(rateText);
    container2_1_1.append(iconStar);
    container2_1_1.append(countRateText)
    container2_1_1.className = [
        'd-flex',
    ]
    const container2_1_2 = document.createElement('div');
    container2_1_2.className = [
        'd-flex',
        'third-color'
    ].join(' ');
    const iconUserCheck = document.createElement('i')
    iconUserCheck.className = [
        'bi',
        'bi-person-fill-check',
        'me-2'
    ].join(' ');
    container2_1.append(container2_1_2)

    container2_1_2.append(iconUserCheck)
    container2_1_2.append(countUsers)

    const container3 = document.createElement('div');
    container.append(container3);
    container3.append(priceSaleCourse);
    container3.append(priceCourse);
    container3.className = [
        'd-flex',
        'px-3',
        'pb-3',
    ].join(' ');


    return linkCourse;
};