import { createCard } from './studied_recently';

export default {
    title: "cards/card_studies_recently",
    tags: ['autodocs'],

};

const Template = () => {
    return createCard();
};

export const page1 = Template.bind({});
