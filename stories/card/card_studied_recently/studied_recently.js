import {createHeading} from "../../header/heading/header";
import {createButton} from "../../button/button";
import {createContent} from "../../header/content/content";
import {createRating} from "../../rating/rating";

export const createCard = () => {

    const container = document.createElement('div');
    container.className = [
        'mw-250',
        'col-md-6',
        'col-xl-4',
        'my-3',
        'list-item',
    ].join(' ');

    const linkImg = document.createElement('a')
    linkImg.href = [
        '#',
    ]
    const courseImg = document.createElement('img');
    courseImg.src = [
        'https://gitiho.com/caches/cc_medium/cou_avatar/2021/05_21/image_05d37a08d453167e96fe8546df55bfd2.jpg',
    ].join(' ');
    courseImg.className = [
        'w-100'
    ].join(' ')
    linkImg.append(courseImg);
    const container1 = document.createElement('a');
    container1.href = [
        '',
    ]
    container.append(container1);
    container1.append(courseImg);

    const container2 = document.createElement('div');
    container2.className = [
        'mt-3',
        'px-3',
    ].join(' ');
    container.append(container2);

    const mainText = document.createElement('a');
    mainText.text = [
        'Kế toán Lương, BHXH, Thuế TNCN'
    ];
    mainText.href = [
        '#',
    ];
    mainText.className = [
        'fw-700',
        'fs-16',
        'mb-2',
        'price-color',
        'line-clamp',
        'course-title',
        'text-decoration-none',
    ].join(' ');
    container2.append(mainText);

    const blockRating = createRating({});
    blockRating.className = [
        'mt-2',
        'd-flex',
    ].join(' ');



    const countRating = document.createElement('div');
    countRating.className = [
        'd-flex',
        'align-items-center'
    ].join(' ');
    const countRating1 = document.createElement('strong');
    const textRating = createContent({
        text: '4.5',
    })
    textRating.className = [
        'mb-0',
        'ms-2'
    ].join(' ');
    const countRating2 = createContent({
        text: '(2 đánh giá)',
    });
    countRating2.className = [
        'mb-0',
        'ms-2',
        'third-color'
    ].join(' ');
    countRating1.append(textRating);
    countRating.append(countRating1);
    countRating.append(countRating2);
    blockRating.append(countRating);
    container2.append(blockRating);




    const progress = document.createElement('div')
    container2.append(progress);
    progress.className = [
        'progress',
        'mt-3'
    ].join(' ')
    const progressbar = document.createElement('div')
    progress.append(progressbar);
    progressbar.className = [ 'progress-bar' ].join(' ')
    progressbar.ariaValueNow = 25;
    progressbar.ariaValueMin = 0;
    progressbar.ariaValueMax = 100;






    const result = document.createElement('div');
    result.className = [
        'd-flex',
        'justify-content-between',
        'align-items-center',
        'my-3'
    ].join(' ');

    container2.append(result);
    const resultPercent = createContent({
        text: "Hoàn thành: 50%",
    });
    resultPercent.className = [
        'my-auto',
        'fs-14',
        'fw-500',
        'third-color',
    ].join(' ')
    result.append(resultPercent)

    const Continue = document.createElement('a');
    result.append(Continue);
    Continue.href = [ '#' ];
    Continue.className = [
        'text-decoration-none',
        'd-flex',
        'align-items-center',
    ].join(' ');
    const resultContinue = createContent({
        text: "Tiếp tục khóa học",
        color:"#007bff",
    });
    resultContinue.className = [
        'third-color',
        'my-auto',
        'fw-500',
        'me-2',
    ].join(' ')
    Continue.append(resultContinue)
    const iconContinue = document.createElement('i');
    Continue.append(iconContinue)
    iconContinue.className = [
        "bi",
        "bi-chevron-right"
    ].join(' ');




    // const secondText = createContent({
    //     text: "Khóa học VBA Excel này sẽ giúp bạn thành thạo VBA giúp tự động hóa quy trình, công việc trên Excel, tối ưu hiệu quả, tăng 200% năng suất làm việc.",
    // });
    // secondText.className = [
    //     'line-clamp',
    //     'line-clamp-2-lines',
    //     'text-content-color',
    // ].join(' ');
    // const rateText = createContent({
    //     text: "4.85"
    // });
    // rateText.className = [
    //     'me-2',
    //     'color_rate',
    // ].join(' ')
    // const countRateText = createContent({
    //     text: "(72)"
    // });
    // countRateText.className = [
    //     'third-color',
    // ];

    // const countUsers = createContent({
    //     text: "18,390 học viên"
    // });
    //
    // countUsers.className = [
    //     'third-color'
    // ];
    // const iconStar = document.createElement('i');
    // iconStar.className = [
    //     'bi',
    //     'bi-star-fill',
    //     'me-2',
    //     'color_rate',
    // ].join(' ')
    //
    //
    //
    // linkCourse.append(container);
    // container.append(courseImg);
    //
    // const container2 = document.createElement('div');
    // container2.className = [
    //     'px-3',
    //     'pt-3',
    // ].join(' ');
    // container.append(container2);
    // container2.append(mainText);
    // container2.append(secondText);
    // const container2_1 = document.createElement('div');
    // container2.append(container2_1);
    // const container2_1_1 = document.createElement('div');
    // container2_1.append(container2_1_1)
    // container2_1.className = [
    //     'd-flex',
    //     'justify-content-between',
    // ].join(' ');
    // container2_1_1.append(rateText);
    // container2_1_1.append(iconStar);
    // container2_1_1.append(countRateText)
    // container2_1_1.className = [
    //     'd-flex',
    // ]
    // const container2_1_2 = document.createElement('div');
    // container2_1_2.className = [
    //     'd-flex',
    //     'third-color'
    // ].join(' ');
    // const iconUserCheck = document.createElement('i')
    // iconUserCheck.className = [
    //     'bi',
    //     'bi-person-fill-check',
    //     'me-2'
    // ].join(' ');
    // container2_1.append(container2_1_2)
    //
    // container2_1_2.append(iconUserCheck)
    // container2_1_2.append(countUsers)
    //
    // const container3 = document.createElement('div');
    // container.append(container3);
    // container3.append(priceSaleCourse);
    // container3.append(priceCourse);
    // container3.className = [
    //     'd-flex',
    //     'px-3',
    //     'pb-3',
    // ].join(' ');


    return container;
};