import './your_course.scss';
import {createHeading} from "../../header/heading/header";
import {createButton} from "../../button/button";
import {createContent} from "../../header/content/content";
import {createRating} from "../../rating/rating";

export const createCard = () => {
    const container = document.createElement('div');
    container.className = [
        'mw-250',
        'col-md-6',
        'col-xl-4',
        'my-3',
        'list-item',
    ].join(' ');

    const courseImg = document.createElement('img');
    courseImg.src = [
        'https://gitiho.com/caches/cc_medium/cou_avatar/2021/05_21/image_05d37a08d453167e96fe8546df55bfd2.jpg',
    ].join(' ');
    courseImg.className = [
        'w-100'
    ].join(' ')

    const container1 = document.createElement('a');
    container1.href = [
        '#',
    ]
    container.append(container1);
    container1.append(courseImg);

    const container2 = document.createElement('div');
    container2.className = [
        'mt-3',
        'px-3',
    ].join(' ');
    container.append(container2);

    const mainText = document.createElement('a');
    mainText.text = [
        'Ebook Tuyệt đỉnh Excel - Khai phá 10 kỹ thuật ứng dụng Excel mà đại học không dạy bạn'
    ];
    mainText.href = [
        '#',
    ];
    mainText.className = [
        'fw-700',
        'fs-16',
        'mb-2',
        'price-color',
        'line-clamp',
        'course-title',
        'text-decoration-none',
    ].join(' ');
    container2.append(mainText);

    const categoryCourse = document.createElement('a');
    categoryCourse.href = [
        '#',
    ]
    categoryCourse.text = [
        'G-LEARNING',
    ]
    categoryCourse.className = [
        'my-1',
        'text-decoration-none',
        'third-color',
    ].join(' ');

    container2.append(categoryCourse);

    const blockRating = createRating({});
    blockRating.className = [
        'mt-2',
        'd-flex',
    ].join(' ');
    container2.append(blockRating);

    const price = document.createElement('div');
    price.className = [
        'd-flex',
        'my-3'
    ].join(' ');
    container2.append(price);
    const priceSaleCourse = createContent({
        text: "69,000đ",
    });
    priceSaleCourse.className = [
        'my-auto',
        'me-3',
        'fs-18',
        'fw-500',
        'price-color',
    ].join(' ')
    const priceCourse = createContent({
        text: "99,000đ",
    });
    priceCourse.className = [
        'text-through',
        'third-color',
        'my-auto'
    ].join(' ')

    price.append(priceSaleCourse)
    price.append(priceCourse)
    // const secondText = createContent({
    //     text: "Khóa học VBA Excel này sẽ giúp bạn thành thạo VBA giúp tự động hóa quy trình, công việc trên Excel, tối ưu hiệu quả, tăng 200% năng suất làm việc.",
    // });
    // secondText.className = [
    //     'line-clamp',
    //     'line-clamp-2-lines',
    //     'text-content-color',
    // ].join(' ');
    // const rateText = createContent({
    //     text: "4.85"
    // });
    // rateText.className = [
    //     'me-2',
    //     'color_rate',
    // ].join(' ')
    // const countRateText = createContent({
    //     text: "(72)"
    // });
    // countRateText.className = [
    //     'third-color',
    // ];

    // const countUsers = createContent({
    //     text: "18,390 học viên"
    // });
    //
    // countUsers.className = [
    //     'third-color'
    // ];
    // const iconStar = document.createElement('i');
    // iconStar.className = [
    //     'bi',
    //     'bi-star-fill',
    //     'me-2',
    //     'color_rate',
    // ].join(' ')
    //
    //
    //
    // linkCourse.append(container);
    // container.append(courseImg);
    //
    // const container2 = document.createElement('div');
    // container2.className = [
    //     'px-3',
    //     'pt-3',
    // ].join(' ');
    // container.append(container2);
    // container2.append(mainText);
    // container2.append(secondText);
    // const container2_1 = document.createElement('div');
    // container2.append(container2_1);
    // const container2_1_1 = document.createElement('div');
    // container2_1.append(container2_1_1)
    // container2_1.className = [
    //     'd-flex',
    //     'justify-content-between',
    // ].join(' ');
    // container2_1_1.append(rateText);
    // container2_1_1.append(iconStar);
    // container2_1_1.append(countRateText)
    // container2_1_1.className = [
    //     'd-flex',
    // ]
    // const container2_1_2 = document.createElement('div');
    // container2_1_2.className = [
    //     'd-flex',
    //     'third-color'
    // ].join(' ');
    // const iconUserCheck = document.createElement('i')
    // iconUserCheck.className = [
    //     'bi',
    //     'bi-person-fill-check',
    //     'me-2'
    // ].join(' ');
    // container2_1.append(container2_1_2)
    //
    // container2_1_2.append(iconUserCheck)
    // container2_1_2.append(countUsers)
    //
    // const container3 = document.createElement('div');
    // container.append(container3);
    // container3.append(priceSaleCourse);
    // container3.append(priceCourse);
    // container3.className = [
    //     'd-flex',
    //     'px-3',
    //     'pb-3',
    // ].join(' ');


    return container;
};