import './user_object.scss';
import {createRating} from "../rating/rating";
import {createContent} from "../header/content/content";

export const createUser =
    ({
         fontSize,
         color,
         text,
     }) => {
        const heading = document.createElement(`h${fontSize}`);
        heading.style.color = color;
        heading.innerText = text;

        const blockUser = document.createElement('div');
        blockUser.className = [
            'border',
            'p-3',
            'mb-2',
        ].join(' ');
        const user = document.createElement('div');
        user.className = [
            'd-flex',
            'mb-3',
        ].join(' ');
        blockUser.append(user);
        const imgUser = document.createElement('img');
        imgUser.width = 36;
        imgUser.height = 36;
        imgUser.src = [
            'https://gitiho.com/caches/ua_small/users/1677586571.jpg',
        ]
        imgUser.className = [
            'my-auto',
            'rounded-circle',
            'me-3',
        ].join(' ');
        user.append(imgUser);

        const rightUser = document.createElement('div');

        user.append(rightUser);

        const nameUser = createContent({
            text: 'Trần Thị Phong Lan'
        });
        nameUser.className = [
            'mb-1',
            'fs-16',
            'fw-700',
        ].join(' ');
        rightUser.append(nameUser);
        const ratingUser = createRating({});
        rightUser.append(ratingUser);


        const textUser = createContent({
            text: 'Khóa học rất bổ ích nhưng học xong vẫn chỉ viết code đơn giản thôi ạ, chắc phải học thêm mấy khóa nữa ạ. Ứng dụng cao trong công việ',
        });
        textUser.className = [
            'fs-16',
            'break-word',
        ].join(' ');
        blockUser.append(textUser);

        return blockUser;
    };