import { createUser } from './user_object';

export default {
    title: "Components/User_object",
    tags: ['autodocs'],
};

const Template = ({ text, ...arg}) => {
    return createUser({ text, ... arg});
};

export const H1 = Template.bind({});
H1.args = {
};