export const createButton = ({
    primary = false,
    backgroundColor,
    borderColor,
    borderRadius='',
    size = 'md',
    rank,
    color,
    label,
    onClick,
     }) => {

    const btn = document.createElement('button');
    // btn.type = 'button';
    btn.style.backgroundColor = backgroundColor;

    btn.style.borderColor = borderColor;

    const mode = primary ? ' btn-primary' : ` btn-secondary`;
    btn.className = ['btn', ` btn-${size}`, mode, ` ${borderRadius}`].join('');

    btn.style.color = color;

    btn.innerText = label;

    btn.addEventListener('click', onClick);

    return btn;
};