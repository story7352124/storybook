import { createButton } from './button';

export default {
  title: "Components/Button",
  tags: ['autodocs'],
  render: ({ label, ...args }) => {
    return createButton({label, ...args});
  },
  argTypes: {
    backgroundColor: { control: 'color' },
    borderColor: { control: 'color' },
    borderRadius: {
      control: {type: 'select'},
      options: ['rounded', 'rounded-circle', 'rounded-pill', 'rounded-0']
    },
    label: { control: 'text' },
    onclick: { action: 'onClick' },
    primary: { control: 'boolean' },
    size: {
      control: { type: 'select' },
      options: [ 'sm', 'md', 'lg'],
    },
  },
};

export const Primary = {
  args: {
    primary: true,
    label: 'Button primary'
  },
};
export const Secondary = {
  args: {
    rank: 'secondary',
    label: 'Button secondary',
  },
};