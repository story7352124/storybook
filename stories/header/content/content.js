import './content.scss';

export const createContent =
    ({
        fontSize,
        fontWeight,
        color,
        text,
    }) => {
    const content = document.createElement('p');
    content.className = [
        `fs-${fontSize}`
    ]
    content.style.fontSize = fontSize;
    content.style.fontWeight = fontWeight;
    content.style.color = color;
    content.innerText = text;

    return content;
};