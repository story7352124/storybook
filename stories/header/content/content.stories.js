import { createContent } from './content';

export default {
    title: "Components/Header/Content",
    tags: ['autodocs'],
    render: ({ text, ...args }) => {
        return createContent({text, ...args});
    },
    argTypes: {
        fontSize: { control: 'number' },
        fontWeight: {
            control: { type: 'select' },
            options: [ '400', '500', '600', '700', '800', '900'],
        },
        color: { control: 'color' },
        text: { control: 'text' },
    },
};

const Template = ({ text, ...arg}) => {
    return createContent({ text, ... arg});
};

export const p = Template.bind({});
p.args = {

};