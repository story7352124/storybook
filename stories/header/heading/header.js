import './header.scss';

export const createHeading =
    ({
        size,
        color,
        text,
     }) => {
    const heading = document.createElement(`h${size}`);
    heading.style.color = color;
    heading.innerText = text;

    return heading;
};