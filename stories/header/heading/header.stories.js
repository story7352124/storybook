import { createHeading } from './header';

export default {
    title: "Components/Header/heading",
    tags: ['autodocs'],
};

const Template = ({ text, ...arg}) => {
    return createHeading({ text, ... arg});
};

export const H1 = Template.bind({});
H1.args = {
    size: 1,
    text: 'i am H1',
};