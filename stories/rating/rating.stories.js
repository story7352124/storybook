import { createRating } from './rating';

export default {
    title: "Components/Rating",
    tags: ['autodocs'],
    render: ({ color, ...args }) => {
        return createRating({color, ...args});
    },
    argTypes: {
        fontSize: { control: 'number' },
        color: { control: 'color' },
    },
};

const Template = ({ color, ...arg}) => {
    return createRating({ color, ... arg});
};

export const p = Template.bind({});
p.args = {

};