import './rating.scss';
import {createContent} from "../header/content/content";
import {createHeading} from "../header/heading/header";

export const createRating =
    ({
         fontSize,
         color,
         text,
     }) => {

        const container = document.createElement('div');
        container.className = [
            'd-flex'
        ]

        const rating = document.createElement('div');
        rating.className = [
            'rating-color',
        ].join(' ');
        rating.style.fontSize = fontSize;
        rating.style.color = color;
        rating.style.innerText = text;

        const starRating = document.createElement('i');
        starRating.className = [
            `fs-${fontSize}`,
            'me-2',
            'bi',
            'bi-star-fill'
        ].join(' ');

        const starRating2 = document.createElement('i');
        starRating2.className = [
            `fs-${fontSize}`,
            'me-2',
            'bi',
            'bi-star-fill'
        ].join(' ');


        const starRating3 = document.createElement('i');
        starRating3.className = [
            `fs-${fontSize}`,
            'me-2',
            'bi',
            'bi-star-fill'
        ].join(' ');

        const starRating4 = document.createElement('i');
        starRating4.className = [
            `fs-${fontSize}`,
            'me-2',
            'bi',
            'bi-star-fill'
        ].join(' ');

        const starRating5 = document.createElement('i');
        starRating5.className = [
            `fs-${fontSize}`,
            'bi',
            'bi-star-half'
        ].join(' ');

        rating.append(starRating);
        rating.append(starRating2);
        rating.append(starRating3);
        rating.append(starRating4);
        rating.append(starRating5);
        container.append(rating);


        return container;
    };