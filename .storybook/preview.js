/** @type { import('@storybook/html').Preview } */
import '../src/index.scss';
import 'bootstrap/dist/js/bootstrap.bundle';
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};
export default preview;